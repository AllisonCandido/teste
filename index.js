var express = require('express');
var app = express();

app.get("/", (request, response) => {
    response.end("ok");
});

app.listen(3000, () => {
    console.log("ouvindo na porta 3000");
});